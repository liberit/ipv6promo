FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html
COPY css/ /usr/share/nginx/html/css/
COPY ipv6fallback.css /usr/share/nginx/html/
COPY isz777nn.txt /usr/share/nginx/html/
